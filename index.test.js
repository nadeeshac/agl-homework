const assert = require('assert');
const sinon = require('sinon');
const fetch = require('node-fetch');
const _ = require('lodash');

const {getData, getSortedCats, groupOwners} = require('./index').testable;

describe('getData', () => {
    it('should fetch the url', (done) => {
        const fakeResult = 'fake';
        const fetchSpy = sinon.stub().returns(new Promise(resolve => {
            return resolve({
                json: () => fakeResult,
            });
        }));

        const fakeUrl = 'http://fake';

        getData(fetchSpy, fakeUrl)
            .then(result => assert(result === fakeResult))
            .then(done);
    });
});

describe('getSortedCats', () => {
    const data = [{
        type: 'foo',
        name: 'bar',
    }, {
        name: 'baz',
        type: 'Cat',
    }, {
        name: 'fuzz',
        type: 'Cat',
    }];

    it('should filter out non-cats', () => {
        const result = getSortedCats(data);
        assert(result.length === 2);
        assert(result.indexOf('baz') > -1);
        assert(result.indexOf('fuzz') > -1);
    });

    it('should sort cats by name', () => {
        const result = getSortedCats(data);
        assert(result[0] === 'baz');
        assert(result[1] === 'fuzz');
    });
});

describe('groupOwners', () => {
    const data = [{
        gender: 'Male',
        pets: [1, 2],
    }, {
        gender: 'Female',
        pets: [3, 4],
    }, {
        gender: 'Male',
        pets: [5, 6],
    }, {
        gender: 'Female',
        pets: null,
    }];

    it('should group owners by gender', () => {
        const getSortedCatsSpy = sinon.spy();
        const result = groupOwners(data, getSortedCatsSpy);
        const genders = _.keys(result);
        assert(genders.indexOf('Male') > -1);
        assert(genders.indexOf('Female') > -1);
    });
});
