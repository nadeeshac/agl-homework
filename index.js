const _ = require('lodash');
const fetch = require('node-fetch');

function getData(fetcher, url) {
  return new Promise((resolve, reject) => {
      fetcher(url)
        .then(body => body.json())
        .then(resolve)
        .catch(reject);
  })
}

const isCat = pet => pet.type === "Cat";

function getSortedCats(petArr) {
  return _(petArr)
    .filter(isCat)
    .map(cat => cat.name)
    .sort()
    .value();
}

function groupOwners(ownerArr, sortCats) {
  return _(ownerArr)
    .groupBy(owner => owner.gender)
    .mapValues(owners => {
        return sortCats(
            _(owners)
                .filter(owner => !_.isEmpty(owner.pets))
                .map(owner => owner.pets)
                .flatten()
                .value()
        );
    })
    .value();
}

function displayData(groupedOwners) {
    _(groupedOwners)
        .forOwn((petNames, ownerGender) => {
            console.log(ownerGender);

            console.log(
                _(petNames)
                    .map(name => ` - ${name}`)
                    .join('\n')
            );
        })
}

function init() {
    const data = 
        getData(fetch, 'http://agl-developer-test.azurewebsites.net/people.json')
        .then(data => groupOwners(data, getSortedCats))
        .then(displayData)
        .catch(e => {
            console.error(e);
        });
}

module.exports.testable = {
    getData,
    getSortedCats,
    groupOwners,
}

module.exports.runnable = {
    init,
}
